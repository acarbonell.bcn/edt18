package com.example.edt18;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rView;
    List<Peak> mPeak = new ArrayList<>();
    private static String JSON_URL = "https://run.mocky.io/v3/61a2a50e-c033-4888-98fa-ce57ac60d435";
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rView = findViewById(R.id.rView);

        getPeaks();
    }

    private void getPeaks(){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for( int i = 0; i < response.length(); i++){
                            try {
                                JSONObject peakObject = response.getJSONObject(i);
                                Peak peak = new Peak();
                                peak.setName(peakObject.getString("name"));
                                peak.setHeight(peakObject.getString("height"));
                                peak.setProminence(peakObject.getString("prominence"));
                                peak.setZone(peakObject.getString("zone"));
                                peak.setUrl(peakObject.getString("url"));
                                peak.setCountry(peakObject.getString("country"));
                                mPeak.add(peak);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        rView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), mPeak);
                        rView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                }
        );
        queue.add(jsonArrayRequest);
    }
}