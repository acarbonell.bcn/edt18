package com.example.edt18;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private List<Peak> mPeak;

    public MyAdapter(Context context, List<Peak> mPeak) {
        this.context = context;
        this.mPeak = mPeak;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name.setText(mPeak.get(position).getName());
        holder.height.setText(mPeak.get(position).getHeight());
        holder.country.setText(mPeak.get(position).getCountry());
        Picasso.get().load(mPeak.get(position).getUrl()).fit().centerCrop().into(holder.url);
    }

    @Override
    public int getItemCount() {
        return mPeak.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView height;
        ImageView url;
        TextView country;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textName);
            height = itemView.findViewById(R.id.textHeight);
            url = itemView.findViewById(R.id.imgUrl);
            country = itemView.findViewById(R.id.textCountry);


        }
    }
}
